import { Link } from "react-router-dom";

function InvestorList({ investors }) {

    if (investors == undefined) return <div></div>;

    const renderData = investors.map((investor) => {
        return <tr key={investor.firmID}>
                  <td>{investor.firmID}</td>
                  <td>{investor.firmName}</td>
                  <td>{investor.firmType}</td>
                  <td>{investor.dateAdded}</td>
                  <td>{investor.address}, {investor.city}, {investor.country}</td>
                  <td>
                    <Link to={`/details?firmId=${investor.firmID}`} className="btn btn-primary">Details</Link>
                  </td>
              </tr>                             
    });

    return <table className="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>AddedDate</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr> 
            </thead>
            <tbody>{renderData}</tbody>
        </table>;

}

export default InvestorList;
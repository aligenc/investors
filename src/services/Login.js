import axios from "axios";

const postLogin = async (token) => {
    
      const {data} = await axios.post('https://api.preqin.com/connect/token', {
        username: 'dummydatafeeds@preqin.com',
        apikey: '8f0bc69bc2a643f8bb8034a15081962e'
      }, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    
    axios.defaults.headers.common['Authorization'] = `Bearer ${data['access_token']}`;

    return data;
};

export default postLogin;
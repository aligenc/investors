import axios from "axios";

const getInvestors = async () => {
    const response = await axios.get('https://api.preqin.com/api/Investor', {
        params: {
            FirmID:'2670,2792,332,3611'
        },
    });

    return response.data;
};

export default getInvestors;
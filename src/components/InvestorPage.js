import { useLocation } from "react-router-dom";
import { useState, useEffect } from 'react';

function InvestorPage() {
    let query = useQuery();

    const [selectedAssetClass, setSelectedAssetClass] = useState('');
    const [commitmentData, setCommitmentData] = useState(null);
    
    let firmId = query.get("firmId");

    useEffect(() => {
      fetchCommitmentData();
    }, [selectedAssetClass]);
  
    async function fetchCommitmentData() {
      try {
        const url = `https://api.preqin.com/api/Investor/commitment/${selectedAssetClass}/${firmId}`;
        console.log(url);
        const response = await fetch(url, { 
        });
  
        if (!response.ok) {
          throw new Error('Failed to fetch commitment data');
        }
  
        const data = await response.json();
        setCommitmentData(data);
      } catch (error) {
        console.error(error);
      }
    }
  
    return (
      <div>
        <h2>Investor Page - {firmId}</h2>
        <div className="col-md-2">        
            <select className="form-select" value={selectedAssetClass} onChange={e => setSelectedAssetClass(e.target.value)}>
                <option value="">Select Asset Class</option>
                <option value="pe">Private Equity</option>
                <option value="pd">Private Debt</option>
                <option value="re">Real Estate</option>
                <option value="inf">Infrastructure</option>
                <option value="nr">Natural Resources</option>
                <option value="hf">Hedge Funds</option>
            </select>
        </div>
        {commitmentData && (
          <div>
            <h3>Commitment Information</h3>
            <p>Asset Class: {selectedAssetClass}</p>
            <p>Investor ID: {firmId}</p>
            <p>Commitment Data: {JSON.stringify(commitmentData)}</p>
          </div>
        )}
      </div>
    );
  }

  function useQuery() {
    return new URLSearchParams(useLocation().search);
}

  export default InvestorPage;
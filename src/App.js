import { useState, useEffect } from 'react';
import {BrowserRouter, Routes, Route, Link} from 'react-router-dom';
import './App.css';
import InvestorList from './components/InvestorList';
import InvestorPage from './components/InvestorPage';
import getInvestors from './services/Investors';
import postLogin from './services/Login';

function App() {
    const [investors, setInvestors] = useState([]);
    
    const login = async () => {
        await postLogin();
    }

    const getInvestorData = async () => {
        const result = await getInvestors();
        setInvestors(result.data);
    }

    useEffect(() => {
      (async () => {
          try {
              await login();
              await getInvestorData();
          } catch (e) {
              console.log(e);
          }
      })();
  }, []);
  

  return (<BrowserRouter>
    
    <nav className="navbar navbar-expand-lg bg-body-tertiary">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Investors</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link to="/" className="nav-link active" aria-current="page">Home</Link>
            </li>
          </ul>
          <form className="d-flex" role="search">
            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
            <button className="btn btn-outline-danger" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <div className="m-3"></div>

      <Routes>
        <Route path="/" element={<InvestorList investors={investors}/>}/>
        <Route path="/details" element={<InvestorPage />}/>         
      </Routes>    
    </BrowserRouter>
  );
}

export default App;
